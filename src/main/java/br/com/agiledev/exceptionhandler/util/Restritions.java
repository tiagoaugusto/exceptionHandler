package br.com.agiledev.exceptionhandler.util;

import java.util.HashMap;
import java.util.Map;

import br.com.agiledev.exceptionhandler.exception.RequiredFieldException;
import br.com.agiledev.exceptionhandler.exception.core.FailException;

/**
 * Agrupa várias verificações de restrições comuns.
 *
 * @author TiagoAugusto
 */
public class Restritions {

	private Restritions() {

	}
	
	/**
	 *
	 * Verifica se o valor é um digito.
	 *
	 * @param idCampo identificador do campo.
	 * @param nomeCampo nome do campo.
	 * @param valor valor.
	 * @throws FailException se o valor não for um digito.
	 */
	public static void digitValue(String idCampo, String nomeCampo, String valor) throws FailException {

		boolean error = false;

		if (valor == null) {
			error = true;
		} else {
			for (int i = 0; i < valor.length() && !error; i++) {
				if (!Character.isDigit(valor.charAt(i))) {
					error = true;
				}
			}
		}

		if (error) {

			Map<String, String> map = new HashMap<String, String>(1);
			String msgError = nomeCampo + " deve ser um valor numérico.";
			map.put(idCampo, msgError);

			throw new FailException(map);
		}

	}

	/**
	 * 
	 * Verifica se o valor foi informado.
	 *
	 * @param idCampo identificador do campo.
	 * @param nomeCampo nome do campo.
	 * @param valor valor.
	 * @exception CampoObrigatorioException Caso o valor informado seja nulo ou vazio.
	 */
	public static void requiredField(String idCampo, String nomeCampo, String valor) {

		if (valor == null || valor.trim().length() < 1) {
			throw new RequiredFieldException(idCampo, nomeCampo);
		}

	}

	/**
	 * 
	 * Verifica se o valor foi informado.
	 *
	 * @param idCampo identificador do campo.
	 * @param valor valor.
	 * @exception RequiredFieldException Caso o valor informado seja nulo ou vazio.
	 */
	public static void requiredField(String idCampo, String nomeCampo, Object valor)
			throws RequiredFieldException {

		if (valor == null) {
			throw new RequiredFieldException(idCampo, nomeCampo);
		}

	}

	/**
	 * 
	 * Verifica se o valor foi informado.
	 *
	 * @param valor valor.
	 * @param msg mensagem apresentada caso a validação falhe.
	 * @throws FailException Caso o valor informado seja nulo ou vazio.
	 */
	public static void notNull(Object valor, String msg) throws FailException {

		if (valor == null) {
			throw new FailException(msg);
		}

	}

	/**
	 * 
	 * Verifica se o valor foi informado, caso não seja uma exceção com a seguinte mensagem será lançada. <br/>
	 * <br/>
	 * Nenhum campo obrigatório foi informado.
	 *
	 * @param valor valor.
	 * @exception FailException Caso o valor informado seja nulo.
	 */
	public static void algumCampoInformado(Object valor) throws FailException {

		if (valor == null) {
			throw new FailException("Nenhum campo obrigatório foi informado.");
		}

	}
}
