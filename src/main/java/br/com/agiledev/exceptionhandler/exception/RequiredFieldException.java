package br.com.agiledev.exceptionhandler.exception;

/**
 * Exceção do tipo de falha para informar que um campo obrigatório não foi informado.
 *
 * @author TiagoAugusto
 */
public class RequiredFieldException extends FailMessageException {

	private static final long serialVersionUID = 1L;

	// Código de erro padrão para regra de negócio.
	private static final String DEFAULT_REQUIRED_FIELD_ERROR_CODE = "RFE";

	/**
	 * Cria a exceção de campo obrigatório para o campo informado.
	 * 
	 * @param idCampo id do campo.
	 */
	public RequiredFieldException(String idCampo, String nomeCampo) {
		super(idCampo, nomeCampo + " é obrigatório.");
		setCode(DEFAULT_REQUIRED_FIELD_ERROR_CODE);
	}

}
