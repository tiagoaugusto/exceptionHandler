package br.com.agiledev.exceptionhandler.exception.core;


/**
 * Exceção técnica que não deve ser tratada pela aplicação e sim por um sistema automático de
 * tratamento de exceção. <br/>
 * Esse tipo de exceção não é lançada por regras de negócio.
 * 
 * @author TiagoAugusto
 */
public class TechnicalException extends ApplicationRuntimeException {

    private static final long serialVersionUID = 1L;

    public TechnicalException(String message) {
        super(message);
    }
    
    public TechnicalException(String message, Throwable e) {
        super(message, e);
    }
    
    public TechnicalException(String message, Throwable e, String code) {
        super(message, e, code);
    }
        
}
