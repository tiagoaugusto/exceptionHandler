package br.com.agiledev.exceptionhandler.exception.core;

/**
 * Exceção que ocorrem em regras de negócio.
 * 
 * @author TiagoAugusto
 */
public class BusinessException extends ApplicationException {

    private static final long serialVersionUID = 1L;
    
    //Código de erro padrão para regra de negócio.
    private static final String DEFAULT_BUSINESS_ERROR_CODE = "BSE";
    
    public BusinessException(String message) {
    	super(message, DEFAULT_BUSINESS_ERROR_CODE);
    }

    public BusinessException(String message, String code) {
    	super(message, code);
    }
}
