package br.com.agiledev.exceptionhandler.exception.core;


/**
 * Classe mãe de todas as exceções n"ao checadas da aplicação.
 * 
 * @author TiagoAugusto
 *
 */
public class ApplicationRuntimeException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	//Código do erro, caso exista algum.
	private String code;
	
	public ApplicationRuntimeException(String message) {
		super(message);
	}
	
	public ApplicationRuntimeException(Throwable thorwable) {
		super(thorwable);
	}

	public ApplicationRuntimeException(String message, Throwable thorwable) {
		this(message, thorwable, null);
	}

	public ApplicationRuntimeException(String message, Throwable thorwable, String code) {
		super(message, thorwable);
		this.code = code;
	}
	
	public ApplicationRuntimeException(String message, String code) {
		super(message);
		this.code = code;
	}
	
	public String getCode() {
		return code;
	}
	
	protected void setCode(String code) {
		this.code = code;
	}

}
