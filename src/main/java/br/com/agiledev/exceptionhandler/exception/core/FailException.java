package br.com.agiledev.exceptionhandler.exception.core;


/**
 * Classe mãe para as exceções não checadas do tipo de falha.
 * <p>
 * Uma exceção do tipo falha representa algum problema causado pela entrada de dados de um ator externo a
 * aplicação. Não existe nada que a aplicação possa fazer para continuar com o processamento, neste caso o ator
 * deve ser avisado do problema ocorrido.
 *
 * @author TiagoAugusto
 */
public class FailException extends ApplicationRuntimeException {

	private static final long serialVersionUID = 1L;

	// Objeto com informações que causaram a falha. Pode ser uma mensagem ou
	// outro objeto mais complexo.
	private Object data;

	public FailException(String message, String codigo) {
		super(message, codigo);
		this.data = message;
	}
	
	public FailException(String message) {
		this(message, null);
	}
	
	public FailException(Object data) {
		this(null, null);
		this.data = data;
	}
	
	public FailException(Object data, String codigo) {
		this(null, codigo);
		this.data = data;
	}
	
	public Object getData() {
		return data;
	}

	protected void setData(Object data) {
		this.data = data;
	}

}
