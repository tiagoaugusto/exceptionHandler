package br.com.agiledev.exceptionhandler.exception;

import java.util.ArrayList;
import java.util.List;

/**
 * Armazena uma lista de mensagens.
 *
 * @author TiagoAugusto
 */
public class Messages {

	private List<Message> mensagens;

	public Messages() {
		mensagens = new ArrayList<Messages.Message>();
	}

	public Messages(String msg) {
		this();
		addMessage(new Message(null, msg));
	}

	public Messages(String codigo, String msg) {
		this();
		addMessage(new Message(codigo, msg));
	}

	public void addMessage(String id, String msg) {
		addMessage(id, null, msg);
	}

	public void addMessage(String id, String codigo, String msg) {
		mensagens.add(new Message(id, codigo, msg));
	}

	public Messages(String[] msgs) {
		this();

		if (msgs != null) {
			for (String msg : msgs) {
				addMessage(new Message(null, msg));
			}
		}
	}

	public void addMessage(Message msg) {
		mensagens.add(msg);
	}

	public List<Message> getMessages() {
		return new ArrayList<Messages.Message>(mensagens);
	}

	public static class Message {

		// Representa o identificado do campo ao qual o erro esta relacionado. Pode ser nulo caso seja uma
		// Mensagem global.
		private String id;

		// Código da mensagem. Pode ser nulo caso não existe um código para a mensagem.
		private String codigo;

		// Descrição da mensagem.
		private String descricao;

		public Message(String id, String codigo, String descricao) {
			this.id = id;
			this.codigo = codigo;
			this.descricao = descricao;
		}

		public Message(String codigo, String descricao) {
			this(null, codigo, descricao);
		}

		public Message(String descricao) {
			this(null, null, descricao);
		}

		public String getCodigo() {
			return codigo;
		}

		public String getDescricao() {
			return descricao;
		}

		public String getId() {
			return id;
		}

	}
}
