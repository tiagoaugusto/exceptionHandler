package br.com.agiledev.exceptionhandler.exception;

import br.com.agiledev.exceptionhandler.exception.core.FailException;

/**
 * Uma exceção do tipo de falha que já contem um ou várias mensagens. O conteúdo desta exceção possui os dados
 * em forma de mensagens.
 *
 * @author TiagoAugusto
 */
public class FailMessageException extends FailException {

	private static final long serialVersionUID = 1L;

	public FailMessageException(String... msgs) {
		super(new Messages(msgs));
	}

	public FailMessageException(String codigo, String msg) {
		super(new Messages(codigo, msg));
	}

	public FailMessageException(Messages msgs) {
		super(msgs);
	}

}
