package br.com.agiledev.exceptionhandler.exception.core;


/**
 * Classe mãe de todas as exceções checadas da aplicação.
 * 
 * @author TiagoAugusto
 *
 */
public class ApplicationException extends Exception {

	private static final long serialVersionUID = 1L;

	//Código do erro, caso exista algum.
	private String codigo;
	
	public ApplicationException(String mensagem) {
		super(mensagem);
	}
	
	public ApplicationException(Throwable thorwable) {
		super(thorwable);
	}

	public ApplicationException(String mensagem, Throwable thorwable) {
		super(mensagem, thorwable);
	}

	public ApplicationException(String mensagem, String codigo) {
		super(mensagem);
		this.codigo = codigo;
	}
	
	public String getCodigo() {
		return codigo;
	}

}
