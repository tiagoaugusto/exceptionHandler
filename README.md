Projeto destinado a agrupar classes de exceção. 

O projeto contém 3 tipos de exceção.
- Exceção de falha.
- Exceção de problema técnico.
- Exceção de regra de negócio.

Exceção de falha
================
Destinado a problemas de entrada de dados.

Exceção de problema técnico
================
Destinado a problemas de técnicos, erros de acesso a banco de dados, perde de 
conexão etc.

Exceção de regra de negócio
================
Destinado a exceção causados por uma condição prevista por uma regra de negócio.
